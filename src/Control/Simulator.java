package Control;

import java.util.Arrays;

import Common.MVC_Controller;
import Data.Command;

public class Simulator implements IControl{
	public Simulator() {
		// TODO Auto-generated constructor stub
		setSupportedCommands();
		
		CmdReader reader = new CmdReader();
		reader.start();
		
	}
	
	public void setSupportedCommands() {
		MVC_Controller.data.registerCmd(new Command() {
			@Override public String getName() { return "helloWorld"; }
			@Override public String getCallSchem() { return "helloWorld"; }
			@Override public int getNbArgs() { return 0; }
			@Override public void Execute(String[] args) { MVC_Controller.view.logMessage("Hello World!"); }
		});
		
		MVC_Controller.data.registerCmd(new Command() {
			@Override public String getName() { return "hello"; }
			@Override public String getCallSchem() { return "hello (name)"; }
			@Override public int getNbArgs() { return 1; }
			@Override public void Execute(String[] args) { MVC_Controller.view.logMessage("Hello " + args[0] + "!"); }
		});
		
		MVC_Controller.data.registerCmd(new Command() {
			@Override public String getName() { return "help"; }
			@Override public String getCallSchem() { return "help"; }
			@Override public int getNbArgs() { return 0; }
			@Override public void Execute(String[] args) { 
				System.out.println("Command list:");
				for (Command supportedCmd : MVC_Controller.data.getAllCommands()) {
					System.out.println(" - " + supportedCmd.getName());
				}
			}
		});
	}

	@Override
	public void executeCmd(String cmd) {
		
		String[] splitted = cmd.split(" ");
		String cmdName = splitted[0];
		String[] args = new String[0];
		if(splitted.length > 1) {
			args = Arrays.copyOfRange(splitted, 1, splitted.length);
		}
		
		for (Command supportedCmd : MVC_Controller.data.getAllCommands()) {
			if(cmdName.compareTo(supportedCmd.getName()) == 0) {
				if(args.length == supportedCmd.getNbArgs()) {
					supportedCmd.Execute(args);
				}
				else {
					MVC_Controller.view.logWarning("Incorrect usage of command, usage schem is:\n      " + supportedCmd.getCallSchem());
				}
				return;
			}
		}
		MVC_Controller.view.logWarning("Unknown command, type \"help\" to get the list of command");
	}
}
