package Data;

public class Fire {

	String m_id;
	int m_intensity;
	Position m_position;
	
	public Fire(int x, int y, int intensity) {
		// TODO Auto-generated constructor stub
		m_position = new Position(x, y);
		m_intensity = intensity;
		m_id = "";
	}
	
	@Override
	public String toString() {
		return "position:" + m_position.toString() + "   -   intensity:" + m_intensity;
	}

}
