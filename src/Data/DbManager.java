package Data;

import java.sql.*;
import java.util.Properties;

public class DbManager {
	private Connection _connSimu;
	private Connection _connEmer;

	
	
	public DbManager() {
		// TODO Auto-generated constructor stub
		String url_baseSimu = "jdbc:postgresql://localhost:5432/postgres";
		String url_baseEmer = "jdbc:postgresql://localhost:5432/postgres";
		Properties props = new Properties();
		props.setProperty("user","postgres");
		props.setProperty("password","williamb");
		_connSimu = null;
		try {
			_connSimu = DriverManager.getConnection(url_baseSimu, props);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		_connEmer = null;
		try {
			_connEmer = DriverManager.getConnection(url_baseEmer, props);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getTableContent(String db, String table) {
		
		EDatabases database;
		database = EDatabases.valueOf(db);
		if(database != null) {
			try {
				Statement st = (database == EDatabases.DB_Emer ? _connEmer : _connSimu).createStatement();
				ResultSet rs = st.executeQuery("SELECT * FROM " + table);
				int row = 0;
				int columnCount = rs.getMetaData().getColumnCount();
				while (rs.next())
				{
				    System.out.print("row " + row + ": (");
				    for (int i = 0; i < columnCount; i++) {
				    	System.out.print(rs.getString(i+1) + ", ");
					}
				    System.out.println(")");
				    row++;
				}
				rs.close();
				st.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
}
