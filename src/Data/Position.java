package Data;

public class Position {
	private int m_x;
	private int m_y;
	
	public Position(int x, int y) {
		// TODO Auto-generated constructor stub
		m_x = x;
		m_y = y;
	}
	public int getM_x() {
		return m_x;
	}
	public int getM_y() {
		return m_y;
	}
	public void setM_x(int m_x) {
		this.m_x = m_x;
	}
	public void setM_y(int m_y) {
		this.m_y = m_y;
	}
	
	
	@Override
	public String toString() {
		return "(" + m_x + ", " + m_y + ")";
	}
}
