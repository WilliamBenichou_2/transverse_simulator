package Data;

import java.text.ParseException;
import java.util.ArrayList;

import Common.MVC_Controller;

public class DataMgr implements IData {

	private ArrayList<Command> m_supportedCommands;
	private DbManager _dbMgr;
	private ArrayList<Truck> _trucks;
	private ArrayList<Firestation> _firestations;
	private ArrayList<Fire> _fires;

	public DataMgr() {
		// TODO Auto-generated constructor stub
		_dbMgr = new DbManager();
		m_supportedCommands = new ArrayList<Command>();

		_trucks = new ArrayList<Truck>();
		_firestations = new ArrayList<Firestation>();
		_fires = new ArrayList<Fire>();

		registerSupportedCommands();
	}

	private void registerSupportedCommands() {
		registerCmd(new Command() {
			@Override
			public String getName() {
				return "getDbContent";
			}

			@Override
			public String getCallSchem() {
				return "getDbContent (database) (table)\n    - database: (DB_Emer or DB_Simu)\n    - table: ex:(public.\"Test\")";
			}

			@Override
			public int getNbArgs() {
				return 2;
			}

			@Override
			public void Execute(String[] args) {
				_dbMgr.getTableContent(args[0], args[1]);
			}
		});

		registerCmd(new Command() {
			@Override
			public String getName() {
				return "createTruck";
			}

			@Override
			public String getCallSchem() {
				return "createTruck (truckId) (firestationId)";
			}

			@Override
			public int getNbArgs() {
				return 2;
			}

			@Override
			public void Execute(String[] args) {
				addTruck(args[0], args[1]);
			}
		});

		registerCmd(new Command() {
			@Override
			public String getName() {
				return "assignTarget";
			}

			@Override
			public String getCallSchem() {
				return "assignTarget (truckId) (fireId)";
			}

			@Override
			public int getNbArgs() {
				return 2;
			}

			@Override
			public void Execute(String[] args) {
				assignTarget(args);
			}

		});

		registerCmd(new Command() {
			@Override
			public String getName() {
				return "createFirestation";
			}

			@Override
			public String getCallSchem() {
				return "createFirestation (id) (posX) (posY)";
			}

			@Override
			public int getNbArgs() {
				return 3;
			}

			@Override
			public void Execute(String[] args) {
				if (!_firestations.contains(args[0])) {
					try {
						_firestations
								.add(new Firestation(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2])));
					} catch (NumberFormatException ex) {
						MVC_Controller.view.logError("X and Y params must be int");
					}

				} else {
					MVC_Controller.view.logError("Firestation already exists");
				}
			}

		});

		registerCmd(new Command() {
			@Override
			public String getName() {
				return "createFire";
			}

			@Override
			public String getCallSchem() {
				return "createFire (posX) (posY) (intensity)";
			}

			@Override
			public int getNbArgs() {
				return 3;
			}

			@Override
			public void Execute(String[] args) {
				try {
					_fires.add(
							new Fire(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2])));
				} catch (NumberFormatException ex) {
					MVC_Controller.view.logError("X and Y params must be int");
				}

			}

		});
		
		
		registerCmd(new Command() {
			@Override
			public String getName() {
				return "listFires";
			}

			@Override
			public String getCallSchem() {
				return "listFires";
			}

			@Override
			public int getNbArgs() {
				return 0;
			}

			@Override
			public void Execute(String[] args) {
				System.out.println("Fires:");
				for (Fire fire : _fires) {
					System.out.println("    -" + fire.toString());
				}

			}

		});

	}

	private void assignTarget(String[] args) {
		if (_trucks.contains(args[0])) {
			Truck truck = _trucks.get(_trucks.indexOf(args[0]));

			if (_fires.contains(args[1])) {
				Fire fire = _fires.get(_fires.indexOf(args[0]));
				truck.setTarget(fire);
			} else
				MVC_Controller.view.logError("fire " + args[1] + " not found");

		} else
			MVC_Controller.view.logError("truck " + args[0] + " not found");
	}

	private void addTruck(String id, String firestationId) {
		// TODO Auto-generated method stub
		if (_firestations.contains(firestationId)) {
			Firestation station = _firestations.get(_firestations.indexOf(firestationId));
			if (!_trucks.contains(id)) {
				_trucks.add(new Truck(id, station));
			}

		}

	}

	@Override
	public void registerCmd(Command cmd) {
		// TODO Auto-generated method stub
		m_supportedCommands.add(cmd);
	}

	@Override
	public Command[] getAllCommands() {
		// TODO Auto-generated method stub
		Command[] cmds = new Command[m_supportedCommands.size()];
		cmds = m_supportedCommands.toArray(cmds);
		return cmds;
	}
}
