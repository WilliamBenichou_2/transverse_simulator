package Data;

public class Truck {
	
	Position m_position;
	Fire m_target;
	private String m_id;
	
	public Truck(String id, Firestation firestation) {
		// TODO Auto-generated constructor stub
		m_id = id;
		m_position = firestation.getM_position();
	}
	
	public void move(Position targetPos) {
		m_position = targetPos;
	}
	
	public void setTarget(Fire fire) {
		m_target = fire;
	}

}
