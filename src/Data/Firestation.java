package Data;

public class Firestation {
	
	String m_id;
	Position m_position;
	
	public Firestation(String id, int x, int y) {
		// TODO Auto-generated constructor stub
		m_id = id;
		m_position = new Position(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj instanceof String) {
			return m_id.compareTo((String)obj) == 0;
		}
		return super.equals(obj);
	}
	
	public String getM_id() {
		return m_id;
	}
	public Position getM_position() {
		return m_position;
	}
}
