package Data;

public interface IData {
	
	static int W = 10, H = 6;
	
	void registerCmd(Command cmd);
	Command[] getAllCommands();
}
