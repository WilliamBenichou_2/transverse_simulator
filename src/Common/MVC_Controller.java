package Common;
import Control.IControl;
import Control.Simulator;
import Data.DataMgr;
import Data.IData;
import View.IView;
import View.Logger;

public class MVC_Controller {
	public static IData data;
	public static IView view;
	public static IControl control;
	
	public MVC_Controller() {
		data = new DataMgr();
		view = new Logger();
		control = new Simulator();
		
	}
}
